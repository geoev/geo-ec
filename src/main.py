#!/usr/bin/env python

"""Publishes multiple messages to a Pub/Sub topic with an error handler."""
import os
from typing import Callable
import json
from concurrent import futures
from google.cloud import pubsub_v1
from geo_db.models.geo_event import GeoEvent


# TODO(developer)
# needs geo-event SA to run
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "/home/victor/.ssh/geo-event.sa.json"
project_id = os.environ['FREETIER_ID']
topic_id = os.environ['GEO_PUBSUB_TOPIC']

publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(project_id, topic_id)
publish_futures = []

def get_callback(
        publish_future: pubsub_v1.publisher.futures.Future,
        data: str,
        timeout: int,
) -> Callable[[pubsub_v1.publisher.futures.Future], None]:
    ''' return a callback function that runs when the future resolves '''
    def callback(publish_future: pubsub_v1.publisher.futures.Future) -> None:
        ''' callback function; just prints results of future '''
        try:
            # Wait 60 seconds for the publish call to succeed.
            print(publish_future.result(timeout=timeout))
        except futures.TimeoutError:
            print(f"Publishing {data} timed out.")

    return callback

if __name__ == '__main__':
    import sys
    try:
        N = int(sys.argv[1])
    except Exception:
        N = 1

    try:
        timeout = int(sys.argv[2])
    except Exception:
        timeout = 1

    for i in range(N):
        # When you publish a message, the client returns a future.
        data = GeoEvent.random_geoevent()
        print(F"about to publish {data} to {topic_path}")
        publish_future = publisher.publish(topic_path, json.dumps(data.dict()).encode("utf-8"))

        # Non-blocking. Publish failures are handled in the callback function.
        publish_future.add_done_callback(get_callback(publish_future, data, timeout))
        publish_futures.append(publish_future)

    # Wait for all the publish futures to resolve before exiting.
    print("waiting for futures...")
    try:
        futures.wait(publish_futures, return_when=futures.ALL_COMPLETED)
    except futures.TimeoutError as e:
        print(F"caught {type(e)}: {e}")

    print(f"Published messages with error handler to {topic_path}.")
